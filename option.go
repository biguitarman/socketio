package socketio

import with "gitlab.com/biguitarman/socketio/internal/option"

type Option = with.Option
type OptionWith = with.OptionWith
